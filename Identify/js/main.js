//var w = window.innerWidth * window.devicePixelRatio,
//    h = window.innerHeight * window.devicePixelRatio;
var game = new Phaser.Game(800,600, Phaser.AUTO, '', { preload: preload, create: create});
var playB,fsB;
function preload() {
	game.load.image('det','assets/DET.png');
	game.load.image('play','assets/wg.png');
	game.load.image('fs','assets/fs.png');
}

function create(){
	//Game background
	var bg=game.add.sprite(0, 0, 'det');
	game.stage.backgroundColor = 0xd4edff;
	bg.scale.set(2);
	//Fullscreen button
	game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
	fsB=game.add.sprite(750, 10, 'fs');
    fsB.inputEnabled = true;
    fsB.input.useHandCursor=true;
    fsB.events.onInputDown.add(gofull, this);	
	//Game name
	var text = "Identify Me";
    var style = { font: "65px Arial", fill: "#020824", align: "center" };
    var t = game.add.text(game.world.centerX-150, 450, text, style);
    //play button
    playB=game.add.sprite(600, 350, 'play');
    playB.scale.set(0.1);
    playB.inputEnabled = true;
    playB.input.useHandCursor=true;
    playB.events.onInputDown.add(openWindow, this);
    var textB = "Play";
    var style = { font: "35px Arial", fill: "#020824", align: "center" };
    var t = game.add.text(670, 360, textB, style);
}

function gofull() {
    if (game.scale.isFullScreen){
        game.scale.stopFullScreen();
    }
    else{
        game.scale.startFullScreen(false);
    }
}

function openWindow(){
	
}

