var game = new Phaser.Game(400, 490, Phaser.AUTO, 'gameDiv');
var mainState = {
	preload:function() { 
        game.stage.backgroundColor = '#71c5cf';
        game.load.image('bird', 'assets/bd.png');
        game.load.image('pipe', 'assets/pipe.png');
    },

    jump: function() {  
    	// Add a vertical velocity to the bird
    	this.bird.body.velocity.y = -350;
    	// Create an animation on the bird
		var animation = game.add.tween(this.bird);
		// Set the animation to change the angle of the sprite to -20° in 100 milliseconds
		animation.to({angle: -20}, 400);
		// And start the animation
		animation.start();
	},

	// Restart the game
	restartGame: function() {    
    	game.state.start('main');
	},

	addOnePipe: function(x, y) {  
    	// Get the first dead pipe of our group
    	var pipe = this.pipes.getFirstDead();
    	// Set the new position of the pipe
    	pipe.reset(x, y);

    	// Add velocity to the pipe to make it move left
    	pipe.body.velocity.x = -200; 

    	// Kill the pipe when it's no longer visible 
    	pipe.checkWorldBounds = true;
    	pipe.outOfBoundsKill = true;
	},

	addRowOfPipes: function() {  
    // Pick where the hole will be
    	var hole = Math.floor(Math.random() * 5) + 1;
    // Add the 6 pipes 
    	for (var i = 0; i < 8; i++)
        	if (i != hole && i != hole + 1) 
            	this.addOnePipe(400, i * 60 + 10);
        this.labelScore.text = this.score;
        this.score += 1;   
	},

    create:function() {
    	game.physics.startSystem(Phaser.Physics.ARCADE);
    	//Add Bird
    	this.bird = this.game.add.sprite(100, 245, 'bird');
    	this.bird.scale.set(0.05);    
    	//Add score
    	this.score = 0;  
		this.labelScore = game.add.text(20, 20, "0", { font: "30px Arial", fill: "#ffffff" });
    	// gravity to the bird to make it fall
	    game.physics.arcade.enable(this.bird);
    	this.bird.body.gravity.y = 1000;  
    	//jump function
    	var spaceKey = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
    	spaceKey.onDown.add(this.jump, this);  
    	//pipes
    	this.pipes = game.add.group(); // Create a group  
		this.pipes.enableBody = true;  // Add physics to the group  
		this.pipes.createMultiple(20, 'pipe'); // Create 20 pipes  
		this.timer = game.time.events.loop(1500, this.addRowOfPipes, this);  
		this.bird.anchor.setTo(-0.2, 0.5);
    },

    update:function() {
    	//Bird too high or low
        if (this.bird.inWorld == false)
        	this.restartGame();
        if (this.bird.angle < 20)  
    		this.bird.angle += 1;
        game.physics.arcade.overlap(this.bird, this.pipes, this.restartGame, null, this);
    }
};

// Add and start the 'main' state to start the game
game.state.add('main', mainState);  
game.state.start('main'); 