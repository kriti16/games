var game = new Phaser.Game(500, 600, Phaser.AUTO, 'game_div');
var main_state = {
    preload: function() {
        // Everything in this function will be executed at the beginning. That’s where we usually load the game’s assets (images, sounds, etc.)
    	game.load.image('hello', 'assets/hello.png');  
    },

    create: function() { 
        // This function will be called after the preload function. Here we set up the game, display sprites, add labels, etc.
    	this.hello_sprite = game.add.sprite(250, 300, 'hello'); 
    },

    update: function() {
        // This is where we will spend the most of our time. This function is called 60 times per second to update the game.
    	this.hello_sprite.angle += 1;  
    } 
}
// And finally we tell Phaser to add and start our 'main' state
game.state.add('main', main_state);  
game.state.start('main');  
